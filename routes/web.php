<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/iniciar', function () {
    return view('iniciar-secion');
});
Route::get('/administrador', function () {
    return view('administrador');
});
Route::get('/empresa', function () {
    return view('empresa');
});
Route::get('/reporte', function () {
    return view('reporte');
});
Route::get('/reporte-oefa', function () {
    return view('oefa-reporte');
});
Route::get('/reporte-empresa', function () {
    return view('empresa-reporte');
});
Route::get('/detalle-administrador', function () {
    return view('detalle-administrador');
});