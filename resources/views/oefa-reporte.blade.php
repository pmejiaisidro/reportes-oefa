@extends('layouts.app')

@section('content')
<div class="container">
    <div class="text-body">
        <h1>
            Administrados con Emergencias Ambientales
        </h1>
    </div>
    <div class="table">
        <div class="table-header">
            <div class="table-header__item table-2">
                <div>
                    <h5>Administrados</h5>
                    <div class="table-triangle table-triangle--asc"></div>
                    <div class="table-triangle table-triangle--des"></div>
                </div><input type="text" placeholder="Escriba aquí" />
            </div>
            <div class="table-header__item table-2">
                <h5>Fecha</h5>
                <div class="table-date">
                    <div class="table-date__title">Desde</div>
                    <div class="table-date__content">
                        <input type="text" placeholder="mes" /><input type="text" placeholder="año" />
                    </div>
                    <div class="table-date__title">Hasta</div>
                    <div class="table-date__content">
                        <input type="text" placeholder="mes" /><input type="text" placeholder="año" />
                    </div>
                </div>
            </div>
            <div class="table-header__item table-2">
                <h5>Sector</h5>
                <div class="form__col form__col--12 flex flex__jc form__select--box">
                    <select class="form__select">
                        <option value="0">Seleccione aquí</option>
                        <option value="1">Hidrocarburos</option>
                        <option value="2">Residuos sólidos</option>
                        <option value="3">Pesquería</option>
                        <option value="4">Agricultura</option>
                    </select>
                </div>
            </div>
            <div class="table-header__item table-2">
                <h5>Actividad</h5>
                <div class="form__col form__col--12 flex flex__jc form__select--box">
                    <select class="form__select">
                        <option value="0">Seleccione aquí</option>
                        <option>Actividad 1</option>
                    </select>
                </div>
            </div>
            <div class="table-header__item table-2">
                <h5>Departamento</h5>
                <div class="form__col form__col--12 flex flex__jc form__select--box">
                    <select class="form__select">
                        <option value="0">Seleccione aquí</option>
                        <option>Lima</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="table-body">
            <div class="table-body__item">
                <div class="table-2 table-body__item-c">Antamina</div>
                <div class="table-2 table-body__item-c">02/2019</div>
                <div class="table-2 table-body__item-c">Hidrocarburos</div>
                <div class="table-2 table-body__item-c">Actividad</div>
                <div class="table-2 table-body__item-c">Departamento</div>
            </div>
            <div class="table-body__item">
                <div class="table-2 table-body__item-c">Morococha</div>
                <div class="table-2 table-body__item-c">05/2018</div>
                <div class="table-2 table-body__item-c">Residuos sólidos</div>
                <div class="table-2 table-body__item-c">Actividad</div>
                <div class="table-2 table-body__item-c">Departamento</div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection