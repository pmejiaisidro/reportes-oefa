@extends('layouts.app')

@section('content')
<div class="container">
    <div class="text-body">
        <h1>PETROLEOS DEL PERU PETROPERU S.A.</h1>
        <p><strong>Dirección:</strong> Mz G Lt 26 República Federal Alemana</p>
        <p><strong>RUC:</strong> 10741506446</p>
        <p><strong>Teléfono:</strong> 928 881 493</p>
        <p><strong>Correo institucional:</strong> grifo@gmail.com</p>
    </div>

    <div class="form__group">
        <div class="form__col form__col--6 flex flex-start js_create">
            <button class="form__btn">Crear nuevo reporte</button>
        </div>
        <div class="form__col form__col--6 flex flex-end js_seeTable">
            <button class="form__btn">Ver Historial de reportes</button>
        </div>
    </div>
    <div class="js_formReport">
        <div class="webform-progressbar">
            <div class="webform-progressbar__content">
                <div class="webform-progressbar__circle active">
                    1
                </div>
                <div class="webform-progressbar__circle">
                    2
                </div>
            </div>
        </div>
        <div class="report active report_1">
            <div class="report__content">
                <div class="report__title">
                    <h2>Formato N° 1</h2>
                    <h1>REPORTE PRELIMINAR DE EMERGENCIAS AMBIENTALES - 001</h1>
                </div>
                <h4>1.  Datos del Administrado</h4>
                <div class="form__group">
                    <div class="form__col form__col--12">
                        <input type="text" name="" class="form__input" placeholder="Nombre o razón social">
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--6 flex flex__jc form__select--box">
                        <select class="form__select">
                            <option value="0">Subsector</option>
                            <option value="1">Electricidad</option>
                            <option value="2">Hidrocarburos</option>
                            <option value="3">Industria</option>
                            <option value="4">Minería</option>
                            <option value="5">Pesquería</option>
                        </select>
                    </div>
                    <div class="form__col form__col--6">
                        <input type="text" name="" class="form__input" placeholder="Actividad">
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--12">
                        <input type="text" name="" class="form__input" placeholder="Domicilio legal">
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--6">
                        <input type="text" name="" class="form__input" placeholder="Distrito">
                    </div>
                    <div class="form__col form__col--6">
                        <input type="text" name="" class="form__input" placeholder="Provincia / Departamento">
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--6 form__ver">
                        <h6>Personas de contacto 1:</h6>
                        <input type="text" name="" class="form__input" placeholder="Nombre">
                        <input type="email" name="" class="form__input" placeholder="Correo">
                        <input type="text" name="" class="form__input" placeholder="Teléfono">
                    </div>
                    <div class="form__col form__col--6 form__ver">
                        <h6>Personas de contacto 2:</h6>
                        <input type="text" name="" class="form__input" placeholder="Nombre">
                        <input type="email" name="" class="form__input" placeholder="Correo">
                        <input type="text" name="" class="form__input" placeholder="Teléfono">
                    </div>
                </div>
                <h4>2.  Del evento</h4>
                <div class="form__group">
                    <div class="form__col form__col--12">
                        <input type="text" name="" class="form__input" placeholder="Nombre de la instalación">
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--4">
                        <input type="text" name="" class="form__input" placeholder="Fecha">
                    </div>
                    <div class="form__col form__col--4">
                        <input type="text" name="" class="form__input" placeholder="Hora de Inicio">
                    </div>
                    <div class="form__col form__col--4">
                        <input type="text" name="" class="form__input" placeholder="Hora de Término">
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--6">
                        <input type="text" name="" class="form__input" placeholder="Área Afectada">
                    </div>
                    <div class="form__col form__col--6">
                        <input type="text" name="" class="form__input" placeholder="Cantidad derramada">
                    </div>
                </div>
                <div class="form__group ">
                    <div class="form__col form__col--6">
                        <h6>Coordenadas UTMDATUM WGS84</h6>
                        <input type="number" name="" class="form__input" placeholder="Este">
                    </div>
                    <div class="form__col form__col--6 flex__end">
                        <input type="number" name="" class="form__input" placeholder="Oeste">
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--4">
                        <input type="text" name="" class="form__input" placeholder="Localidad">
                    </div>
                    <div class="form__col form__col--4">
                        <input type="text" name="" class="form__input" placeholder="Zona">
                    </div>
                    <div class="form__col form__col--4">
                        <input type="text" name="" class="form__input" placeholder="Distrito">
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--6">
                        <input type="text" name="" class="form__input" placeholder="Provincia">
                    </div>
                    <div class="form__col form__col--6">
                        <input type="text" name="" class="form__input" placeholder="Departamento">
                    </div>
                </div>
                <h6 class="space">Posible origen de la emergencia ambiental</h6>
                <div class="form__group">
                    <div class="form__col form__col--6 form__checkbox">
                        <input type="checkbox">
                        <label>Por factores climáticos</label>
                    </div>
                    <div class="form__col form__col--6 form__checkbox">
                        <input type="checkbox">
                        <label>Por falla humana</label>
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--6 form__checkbox">
                        <input type="checkbox">
                        <label>Por factores tecnológicos</label>
                    </div>
                    <div class="form__col form__col--6 form__checkbox">
                        <input type="checkbox">
                        <label>Por acto de terceros</label>
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--6 form__checkbox">
                        <input type="checkbox">
                        <label>Por otros factores</label>
                    </div>
                    <div class="form__col form__col--6 form__checkbox">
                        <input type="checkbox" class="add__item">
                        <label>Precisar</label>
                    </div>
                </div>
            <div class="form__group evento__detallar">
                <div class="form__col form__col--12">
                    <input type="number" name="" class="form__input" placeholder="Detallar">
                </div>
            </div>  
                <div class="form__group">
                    <div class="form__col form__col--12">
                        <h6>Descripción del evento</h6>
                        <textarea class="form__textarea">
                        </textarea>
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--12">
                        <h6>Características del área afectada y los componentes posiblemente afectados (aire, agua, suelo)</h6>
                        <textarea class="form__textarea">
                        </textarea>
                    </div>
                </div>
                <h4>3.  De la persona que reporta</h4>
                <div class="form__group ">
                    <div class="form__col form__col--12">
                        <input type="text" name="" class="form__input" placeholder="Nombre y Apellidos">
                    </div>
                </div>
                <div class="form__group ">
                    <div class="form__col form__col--6">
                        <input type="text" name="" class="form__input" placeholder="DNI o CE">
                    </div>
                    <div class="form__col form__col--6">
                        <input type="text" name="" class="form__input" placeholder="Teléfono">
                    </div>
                </div>
                <div class="form__group ">
                    <div class="form__col form__col--6">
                        <input type="text" name="" class="form__input" placeholder="Cargo">
                    </div>
                    <div class="form__col form__col--6">
                        <input type="text" name="" class="form__input" placeholder="Firma">
                    </div>
                </div>
                <h4>4.  Evidencias que sustentan el reporte</h4>
                <div class="form__group">
                    <div class="form__col form__col--12">
                        <textarea class="form__textarea"></textarea>
                    </div>
                </div>
                <div class="form__group">           
                    <div class="form__col form__col--12 form__img">
                        <input type="file" name="pic" accept="image/*" class="input__file">
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--12 flex flex__jc form__buttons">
                        <button class="form__btn">Guardar</button>
                        <button class="form__btn js__button--company">Enviar</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="report report_2">
            <div class="report__content">
                <div class="report__title">
                    <h2>Formato N° 2</h2>
                    <h1>REPORTE FINAL DE EMERGENCIAS AMBIENTALES - 001</h1>
                </div>
                <h4>1.  Datos del Administrado</h4>
                <div class="form__group">
                    <div class="form__col form__col--12">
                        <input type="text" name="" class="form__input" placeholder="Nombre o razón social">
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--6 flex flex__jc form__select--box">
                        <select class="form__select">
                            <option value="0">Subsector</option>
                            <option value="1">Electricidad</option>
                            <option value="2">Hidrocarburos</option>
                            <option value="3">Industria</option>
                            <option value="4">Minería</option>
                            <option value="5">Pesquería</option>
                        </select>
                    </div>
                    <div class="form__col form__col--6">
                        <input type="text" name="" class="form__input" placeholder="Actividad">
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--12">
                        <input type="text" name="" class="form__input" placeholder="Domicilio legal">
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--6">
                        <input type="text" name="" class="form__input" placeholder="Distrito">
                    </div>
                    <div class="form__col form__col--6">
                        <input type="text" name="" class="form__input" placeholder="Provincia / Departamento">
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--6 form__ver">
                        <h6>Personas de contacto 1:</h6>
                        <input type="text" name="" class="form__input" placeholder="Nombre">
                        <input type="email" name="" class="form__input" placeholder="Correo">
                        <input type="text" name="" class="form__input" placeholder="Teléfono">
                    </div>
                    <div class="form__col form__col--6 form__ver">
                        <h6>Personas de contacto 2:</h6>
                        <input type="text" name="" class="form__input" placeholder="Nombre">
                        <input type="email" name="" class="form__input" placeholder="Correo">
                        <input type="text" name="" class="form__input" placeholder="Teléfono">
                    </div>
                </div>
                <h4>2.  Del evento</h4>
                <div class="form__group">
                    <div class="form__col form__col--4">
                        <input type="text" name="" class="form__input" placeholder="Fecha">
                    </div>
                    <div class="form__col form__col--4">
                        <input type="text" name="" class="form__input" placeholder="Hora de Inicio">
                    </div>
                    <div class="form__col form__col--4">
                        <input type="text" name="" class="form__input" placeholder="Hora de Término">
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--12">
                        <input type="text" name="" class="form__input" placeholder="Lugar donde ocurrió">
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--4">
                        <input type="text" name="" class="form__input" placeholder="Localidad">
                    </div>
                    <div class="form__col form__col--4">
                        <input type="text" name="" class="form__input" placeholder="Sector">
                    </div>
                    <div class="form__col form__col--4">
                        <input type="text" name="" class="form__input" placeholder="Distrito">
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--6">
                        <input type="text" name="" class="form__input" placeholder="Provincia">
                    </div>
                    <div class="form__col form__col--6">
                        <input type="text" name="" class="form__input" placeholder="Departamento">
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--12">
                        <h6>DESCRIPCION DETALLADA DEL EVENTO</h6>
                        <textarea class="form__textarea">
                        </textarea>
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--12">
                        <h6>CAUSAS QUE ORIGINARON EL EVENTO</h6>
                        <textarea class="form__textarea">
                        </textarea>
                    </div>
                </div>
                <div class="form__detail">
                    <ol>
                        <li>La descripción deberá hacerse de manera detallada precisando secuencialmente el tipo y la(s) causa(s) del accidente, fecha y hora de la ocurrencia, las acciones y coordinaciones realizadas, los daños generados, las personas afectadas y las consecuencias respectivas. En caso se consigne información diferente a laindicada en el Reporte Preliminar, deberán sustentarse las variaciones, deigual forma para cualquier variación de datos en el presente reporte.</li>
                    </ol>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--12">
                        <h6>Describir las condiciones climáticas durante y después de ocurrido el evento</h6>
                        <textarea class="form__textarea">
                        </textarea>
                    </div>
                </div>
                <h6 class="space">¿Se puso en marchael Plan de Contingencias?</h6>
                <div class="form__group">
                    <div class="form__col form__col--6 form__checkbox">
                        <input type="checkbox">
                        <label>Si</label>
                    </div>
                    <div class="form__col form__col--6 form__checkbox">
                        <input type="checkbox">
                        <label class="add__item">No</label>
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--12">
                        <h6>Explicar</h6>
                        <textarea class="form__textarea">
                        </textarea>
                    </div>
                </div>
                <h4>3.  Consecuencias del evento</h4>
                <div class="form__group">
                    <div class="form__col form__col--12">
                        <h6>3.1. IMPACTOS Y/O DAÑOS AMBIENTALES3</h6>
                        <textarea class="form__textarea">
                        </textarea>
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--12">
                        <h6>3.2. AFECTACIÓN A LA SALUD DE LAS PERSONAS DERIVADA DE LOS IMPACTOS Y/O DAÑOS AMBIENTALES</h6>
                        <textarea class="form__textarea">
                        </textarea>
                    </div>
                </div>
                <h6 class="space">3.3. DERRAME O FUGA </h6>
                <div class="form__group">
                    <div class="form__col form__col--12">
                        <input type="text" name="" class="form__input" placeholder="Tipo de productos">
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--4 form__checkbox">
                        <input type="checkbox">
                        <label>Líquido</label>
                    </div>
                    <div class="form__col form__col--4 form__checkbox">
                        <input type="checkbox">
                        <label class="add__item">Sólido</label>
                    </div>
                    <div class="form__col form__col--4 form__checkbox">
                        <input type="checkbox">
                        <label class="add__item">Gaseoso</label>
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--12">
                        <input type="text" name="" class="form__input" placeholder="Tipo de productos">
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--4 form__checkbox">
                        <input type="checkbox">
                        <label>Líquido</label>
                    </div>
                    <div class="form__col form__col--4 form__checkbox">
                        <input type="checkbox">
                        <label class="add__item">Sólido</label>
                    </div>
                    <div class="form__col form__col--4 form__checkbox">
                        <input type="checkbox">
                        <label class="add__item">Gaseoso</label>
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--12">
                        <input type="text" name="" class="form__input" placeholder="Tipo de productos">
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--4 form__checkbox">
                        <input type="checkbox">
                        <label>Líquido</label>
                    </div>
                    <div class="form__col form__col--4 form__checkbox">
                        <input type="checkbox">
                        <label class="add__item">Sólido</label>
                    </div>
                    <div class="form__col form__col--4 form__checkbox">
                        <input type="checkbox">
                        <label class="add__item">Gaseoso</label>
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--4 form__checkbox">
                        <input type="checkbox">
                        <label>Líquido</label>
                    </div>
                    <div class="form__col form__col--4 form__checkbox">
                        <input type="checkbox">
                        <label class="add__item">Sólido</label>
                    </div>
                    <div class="form__col form__col--4 form__checkbox">
                        <input type="checkbox">
                        <label class="add__item">Gaseoso</label>
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--6">
                        <h6>Volumen aproximado del derrame o fuga</h6>
                        <input type="text" name="" class="form__input" placeholder="_______Galones">
                    </div>
                    <div class="form__col form__col--6 flex__end ">
                        <input type="text" name="" class="form__input" placeholder="_______Galones">
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--6">
                        <h6>Volumen aproximado del derrame o fuga</h6>
                        <input type="text" name="" class="form__input" placeholder="_______Galones">
                    </div>
                    <div class="form__col form__col--6 flex__end ">
                        <input type="text" name="" class="form__input" placeholder="_______Galones">
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--12">
                        <input type="text" name="" class="form__input" placeholder="Áreas Involucrada aproximada (m2)">
                    </div>
                </div>
                <div class="form__group"> 
                    <div class="form__col form__col--12">
                        <h6>Especificar Producto(s)</h6>
                        <textarea class="form__textarea"></textarea>
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--12">
                        <h6>Detalles de las acciones realizadas por el Administrado </h6>
                        <textarea class="form__textarea"></textarea>
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--12">
                        <input type="number" name="" class="form__input" placeholder="Cantidad de la sustancia, material o residuo recuperado">
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--12">
                        <input type="number" name="" class="form__input" placeholder="Cantidad de la sustancia, material o residuo NO recuperado:">
                    </div>
                </div>
                <h4>4. Acciones correctivas (Para corregir y/o evitar el evento descrito y sus consecuencias)</h4>
                <div class="form__group">
                    <div class="form__col form__col--12 form__ver">
                        <h6>Medidas a adoptar:</h6>
                        <input type="text" name="" class="form__input" placeholder="1">
                        <input type="text" name="" class="form__input" placeholder="2">
                        <input type="text" name="" class="form__input" placeholder="3">
                    </div>
                </div>
                <div class="form__detail">
                    <ol>
                        <li>Describa las condiciones ambientales que prevalecían al momento de la emergencia,incluyendo aquellas que ayuden a explicar el comportamiento de las sustancias o energía liberada (dirección del viento, temperatura ambiente, humedad relativa).</li>
                        <li>PIndicar características del daño de cada área afectada: agua, suelo, flora, fauna, reserva natural,restos arqueológicos, bofedales, etc.</li>
                        <li>Precisar si son hidrocarburos líquidos, gaseosos, efluentes, relaves, aceite dieléctrico, sustancias químicas,etc</li>
                        <li>Se consignará en “m2” enlos casos que corresponda.</li>
                    </ol>
                </div>
                <h4 class="space">5. Empresa Prestadora de Servicios de Residuos Sólidos (Transporte y Disposición Final)</h4>
                <div class="form__group">
                    <div class="form__col form__col--12">
                        <textarea class="form__textarea"></textarea>
                    </div>
                </div>
                <div class="tooltip-report">
                    <h4 class="space">6. Estado de la instalación o medio de transporte después de las emergencias <i class="far fa-question-circle variant2"></i></h4>
                    <div class="tooltip-report__card">
                        <p>Pueden ser:
                            <br>
                            <strong>- Operativo: </strong>Cuando no hasufrido daños que impidan el normal desarrollo de sus operaciones.
                            <br>
                            <strong>- Inoperativo Parcial:</strong> Cuando una parte de las instalaciones hasido afectadapor el evento peroque no conllevaal cese de sus operaciones de manera total.
                            <br>
                            <strong>- Inoperativo Total:</strong>Cuando la unidad no está en condiciones de seguir operando de manera definitiva</li>
                        </p>
                    </div>
                </div>
                <form>
                    <div class="form__group">
                        <div class="form__col form__col--4 form__checkbox">
                            <input type="checkbox">
                            <label>OPERATIVO </label>
                        </div>
                        <div class="form__col form__col--4 form__checkbox">
                            <input type="checkbox">
                            <label class="add__item">INOPERATIVO </label>
                        </div>
                        <div class="form__col form__col--4 form__checkbox">
                            <input type="checkbox">
                            <label class="add__item">INOPERATIVO TOTAL </label>
                        </div>
                    </div>
                </form>
                <h4 class="space">7.  Documentación que se adjunta:</h4>
                <div class="form__group">
                    <div class="form__col form__col--12">
                        <input type="text" name="" class="form__input" placeholder="Croquis del lugar de la emergencia (obligatorio siempre) con georeferencia WGS84">

                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--12">
                        <input type="text" name="" class="form__input" placeholder="Fotografías a color (obligatorio siempre) con georeferencia WGS84">
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--12">
                        <input type="text" name="" class="form__input" placeholder="Otros (especificar)">
                    </div>
                </div>
                <div class="form__group">
                    <div class="form__col form__col--12 flex flex__jc form__buttons">
                        <button class="form__btn">Guardar</button>
                        <button class="form__btn">Enviar</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="js_formTable table">
        <div class="table-header">
            <div class="table-header__item table-25"><div><h5>Reporte</h5><div class="table-triangle table-triangle--asc"></div><div class="table-triangle table-triangle--des"></div></div><input type="text" id="js_input" placeholder="Escriba aquí"/></div>
            <div class="table-header__item table-40">
            <h5>Fecha</h5>
            <div class="table-date">
                <div class="table-date__title">Desde</div>
                <div class="table-date__content">
                <input id="searchFrom" class="searchInput" type="text" placeholder="mes/año"/>
                </div>
                <div class="table-date__title">Hasta</div>
                <div class="table-date__content">
                <input id="searchTo" class="searchInput" type="text" placeholder="mes/año"/>
                </div>
            </div>
            </div>
            <div class="table-header__item table-25">
                <h5>Sector</h5>
                <div class="form__col form__col--12 flex flex__jc form__select--box">
                    <select class="form__select" id="js_select_sector">
                        <option value="0">Seleccione aquí</option>
                        <option value="Hidrocarburos">Hidrocarburos</option>
                        <option value="Residuos sólidos">Residuos sólidos</option>
                        <option value="Pesquería">Pesquería</option>
                        <option value="Agricultura">Agricultura</option>
                    </select>
                </div>
            </div>
            <div class="table-header__item table-25">
                <h5>Actividad</h5>
                <div class="form__col form__col--12 flex flex__jc form__select--box">
                    <select class="form__select" id="js_select_actividad">
                        <option value="0">Seleccione aquí</option>
                        <option value="Transporte">Transporte</option>
                        <option value="Comercialización UME">Comercialización UME</option> 
                        <option value="Área Degradada">Área Degradada</option>
                        <option value="Disposición Final">Disposición Final</option> 
                        <option value="Cultivo de Trucha">Cultivo de Trucha</option> 
                        <option value="Campo de cultivo">Campo de cultivo</option> 
                    </select>
                </div>
            </div>
        </div>
        <div class="table-body">
        <div class="table-body__item js_table_body">
            <div class="table-25 table-body__item-c"><div data-toggle="modal" style="cursor:pointer;" data-target="#reporte-preliminar">R-P-001</div></div>
            <div class="table-40 table-body__item-c js_table_date">07/2019</div>
            <div class="table-25 table-body__item-c">Hidrocarburos</div>
            <div class="table-25 table-body__item-c">Transporte</div>
            </div>
            <div class="table-body__item js_table_body">
            <div class="table-25 table-body__item-c"><div data-toggle="modal" style="cursor:pointer;" data-target="#reporte-final">R-F-001</div></div>
            <div class="table-40 table-body__item-c js_table_date">07/2019</div>
            <div class="table-25 table-body__item-c">Hidrocarburos</div>
            <div class="table-25 table-body__item-c">Transporte</div>
            </div>
            <div class="table-body__item js_table_body">
            <div class="table-25 table-body__item-c"><div data-toggle="modal" style="cursor:pointer;" data-target="#reporte-preliminar">R-P-002</div></div>
            <div class="table-40 table-body__item-c js_table_date">04/2019</div>
            <div class="table-25 table-body__item-c">Hidrocarburos</div>
            <div class="table-25 table-body__item-c">Comercialización UME</div>
            </div>
        <div class="table-body__item js_table_body">
            <div class="table-25 table-body__item-c"><div data-toggle="modal" style="cursor:pointer;" data-target="#reporte-preliminar">R-F-002</div></div>
            <div class="table-40 table-body__item-c js_table_date">03/2018</div>
            <div class="table-25 table-body__item-c">Residuos sólidos</div>
            <div class="table-25 table-body__item-c">Disposición Final</div>
            </div>
            <div class="table-body__item js_table_body">
            <div class="table-25 table-body__item-c"><div data-toggle="modal" style="cursor:pointer;" data-target="#reporte-preliminar">R-P-003</div></div>
            <div class="table-40 table-body__item-c js_table_date">03/2018</div>
            <div class="table-25 table-body__item-c">Pesquería</div>
            <div class="table-25 table-body__item-c">Cultivo de Trucha</div>
            </div>
            <div class="table-body__item js_table_body">
            <div class="table-25 table-body__item-c"><div data-toggle="modal" style="cursor:pointer;" data-target="#reporte-preliminar">R-F-003</div></div>
            <div class="table-40 table-body__item-c js_table_date">02/2018</div>
            <div class="table-25 table-body__item-c">Agricultura</div>
            <div class="table-25 table-body__item-c">Campo de cultivo</div>
            </div>
        </div>
    </div>
</div>
@include('reporte')
@endsection