@extends('layouts.app')

@section('content')
  <div class="container text-body">
      <h1 style="margin: 30px 0 0 0;">
          Administrados con Emergencias Ambientales
      </h1>
  </div>
<div class="table">
  <div class="table-header">
    <div class="table-header__item table-2"><div><h5>Administrados</h5><div class="table-triangle table-triangle--asc"></div><div class="table-triangle table-triangle--des"></div></div><input type="text" id="js_input" placeholder="Escriba aquí"/></div>
    <div class="table-header__item table-3">
      <h5>Fecha</h5>
      <div class="table-date">
        <div class="table-date__title">Desde</div>
        <div class="table-date__content">
          <input id="searchFrom" class="searchInput" type="text" placeholder="mes/año"/>
        </div>
        <div class="table-date__title">Hasta</div>
        <div class="table-date__content">
          <input id="searchTo" class="searchInput" type="text" placeholder="mes/año"/>
         </div>
      </div>
    </div>
    <div class="table-header__item table-1">
        <h5>Sector</h5>
        <div class="form__col form__col--12 flex flex__jc form__select--box">
            <select class="form__select" id="js_select_sector">
                <option value="0">Seleccione aquí</option>
                <option value="Hidrocarburos">Hidrocarburos</option>
                <option value="Residuos sólidos">Residuos sólidos</option>
                <option value="Pesquería">Pesquería</option>
                <option value="Agricultura">Agricultura</option>
            </select>
        </div>
    </div>
    <div class="table-header__item table-2">
        <h5>Actividad</h5>
        <div class="form__col form__col--12 flex flex__jc form__select--box">
            <select class="form__select" id="js_select_actividad">
                <option value="0">Seleccione aquí</option>
                <option value="Transporte">Transporte</option>
                <option value="Comercialización UME">Comercialización UME</option> 
                <option value="Área Degradada">Área Degradada</option>
                <option value="Disposición Final">Disposición Final</option> 
                <option value="Cultivo de Trucha">Cultivo de Trucha</option> 
                <option value="Campo de cultivo">Campo de cultivo</option> 
            </select>
        </div>
    </div>
   <div class="table-header__item table-1">
       <h5>Departamento</h5>
       <div class="form__col form__col--12 flex flex__jc form__select--box">
           <select class="form__select" id="js_select_departamento">
               <option value="0">Seleccione aquí</option>
               <option value="Lima">Lima</option>
               <option value="Junín">Junín</option>
               <option value="Arequipa">Arequipa</option>
               <option value="Tacna">Tacna</option>
            </select>
        </div>
    </div>
 </div>
 <div class="table-body">
   <div class="table-body__item js_table_body">
     <div class="table-2 table-body__item-c"><a href="http://reportes-oefa.test/detalle-administrador">A & M TRANSPORTES S.R.L.</a></div>
     <div class="table-3 table-body__item-c js_table_date">07/2019</div>
      <div class="table-1 table-body__item-c">Hidrocarburos</div>
      <div class="table-2 table-body__item-c">Transporte</div>
      <div class="table-1 table-body__item-c">Lima</div>
    </div>
    <div class="table-body__item js_table_body">
     <div class="table-2 table-body__item-c"><a href="http://reportes-oefa.test/detalle-administrador" >MAYHUIRI ROMAN HEBERTH</a></div>
     <div class="table-3 table-body__item-c js_table_date">05/2019</div>
      <div class="table-1 table-body__item-c">Residuos sólidos</div>
      <div class="table-2 table-body__item-c">Área Degradada</div>
      <div class="table-1 table-body__item-c">Tacna</div>
    </div>
    <div class="table-body__item js_table_body">
     <div class="table-2 table-body__item-c"> <a href="http://reportes-oefa.test/detalle-administrador" >VETRA PERU SAC </a></div>
     <div class="table-3 table-body__item-c js_table_date">04/2019</div>
      <div class="table-1 table-body__item-c">Hidrocarburos</div>
      <div class="table-2 table-body__item-c">Comercialización UME</div>
      <div class="table-1 table-body__item-c">Arequipa</div>
    </div>
   <div class="table-body__item js_table_body">
     <div class="table-2 table-body__item-c"><a href="http://reportes-oefa.test/detalle-administrador" > BA SERVICIOS AMBIENTALES S.A.C. </a></div>
     <div class="table-3 table-body__item-c js_table_date">03/2018</div>
      <div class="table-1 table-body__item-c">Residuos sólidos</div>
      <div class="table-2 table-body__item-c">Disposición Final</div>
      <div class="table-1 table-body__item-c">Junín</div>
    </div>
    <div class="table-body__item js_table_body">
     <div class="table-2 table-body__item-c"><a href="http://reportes-oefa.test/detalle-administrador" >ACUACULTURA Y PESCA S.A.C</a></div>
     <div class="table-3 table-body__item-c js_table_date">03/2018</div>
      <div class="table-1 table-body__item-c">Pesquería</div>
      <div class="table-2 table-body__item-c">Cultivo de Trucha</div>
      <div class="table-1 table-body__item-c">Lima</div>
    </div>
    <div class="table-body__item js_table_body">
     <div class="table-2 table-body__item-c"><a href="http://reportes-oefa.test/detalle-administrador" >CANCHARI RIVERA CARLOS ALBERTO</a></div>
     <div class="table-3 table-body__item-c js_table_date">02/2018</div>
      <div class="table-1 table-body__item-c">Agricultura</div>
      <div class="table-2 table-body__item-c">Campo de cultivo</div>
      <div class="table-1 table-body__item-c">Lima</div>
    </div>
  </div>
</div>

@endsection
