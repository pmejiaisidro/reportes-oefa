<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/28ba03f545.js"></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                <img src="https://i.postimg.cc/cJx0tRQ9/oefa-logo.png" alt="">
                    <!-- {{ config('app.name', 'Laravel') }} -->
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                        @endif
                        @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function() {
            $(".contact__hipervinculo").on("click", function(e) {
                var caja = $(this).data("box");
                console.log(caja);

                $(".contact__place").hide();
                $(caja).show();
                $(".contact__hipervinculo").removeClass("contact__hipervinculo--active");
                $(this).addClass("contact__hipervinculo--active");
                $(".login__tabs li").removeClass("active");
                $(this).parent().addClass("active");
                e.preventDefault();
            });  


            $('.add__item').on( 'click', function() {
                if( $(this).is(':checked') ){
                    $(".evento__detallar").addClass("show");
                } else {
                    $(".evento__detallar").removeClass("show");
                }
            }); 
            update(".table-triangle--asc",".table-triangle--des");
            update(".table-triangle--des",".table-triangle--asc");
            function update(option1,option2) { 
                $(option1).click(function() {
                    if($(option2).hasClass('hidden')){
                        $(option2).removeClass('hidden');
                        $(option2).addClass('active');
                        $(option1).removeClass('active');
                    }else{
                        $(option1).removeClass('hidden');
                        $(option1).addClass('active');
                        $(option2).addClass('hidden');
                    }
                });  
            } 
            $("#js_input").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $(".js_table_body").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
            $(".searchInput").on("input", function() {
                var from = stringToDate($("#searchFrom").val());
                var to = stringToDate($("#searchTo").val());
                $(".js_table_body").each(function() {
                    var row = $(this);
                    var date = stringToDate(row.find("div").eq(1).text());
                    var show = true;
                    if (from && date < from)
                    show = false;
                    if (to && date > to)
                    show = false;

                    if (show)
                    row.show();
                    else
                    row.hide();
                });
            });

            function stringToDate(s) {
                var ret = NaN;
                var parts = s.split("/");
                date = new Date(parts[0], parts[1]);
                console.log(date);
                if (!isNaN(date.getTime())) {
                    ret = date;
                }
                return ret;
            }
            select("#js_select_sector");
            select("#js_select_actividad");
            select("#js_select_departamento");
            function select(value){
                $(value).on("change", function() {
                    var value = $(this).val().toLowerCase();
                    $(".js_table_body").filter(function() {
                        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                    });
                });
            }  


            $(".input__file").change(function(e) {

                for (var i = 0; i < e.originalEvent.srcElement.files.length; i++) {
                    
                    var file = e.originalEvent.srcElement.files[i];
                    
                    var img = document.createElement("img");
                    var reader = new FileReader();
                    reader.onloadend = function() {
                         img.src = reader.result;
                    }
                    reader.readAsDataURL(file);
                    $(".input__file").after(img);
                }
            });
            $(".js_formReport").hide();
            $(".js_formTable").hide();
            btn(".js_create",".js_formReport",".js_formTable");
            btn(".js_seeTable",".js_formTable",".js_formReport");
            function btn(btn,show,hide){
                $(btn).on("click", function(e) {
                    $(hide).hide();
                    $(show).show();
                    
                });
            }

            $(".js__button--company").on( 'click', function() {
                $(".report_1").hide();
                $(".report_2").show();
                $("html, body").animate({ scrollTop: 300 }, "slow");
                $(".webform-progressbar__circle").removeClass("active");
                $(".webform-progressbar__circle:nth-child(2)").addClass("active");
            }); 
        });
        new WOW().init();
        var piesiteFired = 0;
        $(document).ready(function() {
            var $win = $(window),
                $win_height = $(window).height(),
                windowPercentage = $(window).height() * 0.9;
            $win.on("scroll", scrollReveal);
            function scrollReveal() {
                var scrolled = $win.scrollTop();
                $(".trigger").each(function() {
                    var $this = $(this),
                        offsetTop = $this.offset().top;
                    if (
                        scrolled + windowPercentage > offsetTop ||
                        $win_height > offsetTop
                    ) {
                        $(this).each(function(key, bar) {
                            var percentage = $(this).data("percentage");
                            $(this).css("height", percentage + "%"); 
                            $(this).prop("Counter", 0).animate(
                                {
                                    Counter: $(this).data("percentage")
                                },
                                {
                                    duration: 2000,
                                    easing: "swing",
                                    step: function(now) {
                                        $(this).text(Math.ceil(now));
                                    }
                                }
                            );
                        });

                    } else {
                        $(this).each(function(key, bar) {
                            $(this).css("height", 0);
                        });
                    }    
                    
                });
                $(".piesite").each(function() {
                    var $this = $(this),
                        offsetTop = $this.offset().top;
                    if (
                        scrolled + windowPercentage > offsetTop ||
                        $win_height > offsetTop
                    ) {
                        if (piesiteFired == 0) {
                            timerSeconds = 3;
                            timerFinish = new Date().getTime() + timerSeconds * 1000;
                            $(".piesite").each(function(a) {
                                console.log(a);
                                pie = $("#pie_" + a).data("pie");
                                timer = setInterval(
                                    "stoppie(" + a + ", " + pie + ")",
                                    0
                                );
                            });
                            piesiteFired = 1;
                        }
                    } else {
                        $(".piesite").each(function() {
                            piesiteFired = 0;
                        });
                    }
                });
            }
            scrollReveal();
        });

        var timer;
        var timerFinish;
        var timerSeconds;

        function drawTimer(c, a) {
            $("#pie_" + c).html(
                '<div class="percent"></div><div id="slice"' +
                    (a > 50 ? ' class="gt50"' : "") +
                    '><div class="pie"></div>' +
                    (a > 50 ? '<div class="pie fill"></div>' : "") +
                    "</div>"
            );
            var b = 360 / 100 * a;
            $("#pie_" + c + " #slice .pie").css({
                "-moz-transform": "rotate(" + b + "deg)",
                "-webkit-transform": "rotate(" + b + "deg)",
                "-o-transform": "rotate(" + b + "deg)",
                transform: "rotate(" + b + "deg)"
            });
            a = Math.floor(a * 100) / 100;
            arr = a.toString().split(".");
            intPart = arr[0];
            $("#pie_" + c + " .percent").html(
                '<span class="int">' +
                    intPart +
                    "</span>" +
                    '<span class="symbol">%</span>'
            );
        }
        function stoppie(d, b) {
            var c = (timerFinish - new Date().getTime()) / 1000;
            var a = 100 - c / timerSeconds * 100;
            a = Math.floor(a * 100) / 100;
            if (a <= b) {
                drawTimer(d, a);
            } else {
                b = $("#pie_" + d).data("pie");
                arr = b.toString().split(".");
                $("#pie_" + d + " .percent .int").html(arr[0]);
            }
        }
       
    </script>
</body>

</html>