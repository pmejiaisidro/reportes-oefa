@extends('layouts.app')

@section('content')

<div class="container">

   <form>
       <div class="form__group">
           <div class="form__col form__col--12">
               <input type="number" name="" class="form__input" placeholder="Ingrese su RUC">
           </div>
        </div>
        <div class="form__group">
           <div class="form__col form__col--12">
               <input type="email" name="" class="form__input" placeholder="Ingrese su correo">
           </div>
        </div>
        <div class="form__group">
           <div class="form__col form__col--12 flex flex__jc form__select--box">
                <select class="form__select">
                    <option value="0">Subsector</option>
                    <option value="1">Electricidad</option>
                    <option value="2">Hidrocarburos</option>
                    <option value="3">Industria</option>
                    <option value="4">Minería</option>
                    <option value="5">Pesquería</option>
                </select>
           </div>           
       </div>
       <div class="form__group">
           <div class="form__col form__col--6 form__checkbox">
               <input type="checkbox">
               <label>Por factores climáticos</label>
           </div>
           <div class="form__col form__col--6 form__checkbox">
               <input type="checkbox">
               <label>Por falla humana</label>
           </div>
        </div>
        <div class="form__group">
           <div class="form__col form__col--6 form__checkbox">
               <input type="checkbox">
               <label>Por factores tecnológicos</label>
           </div>
           <div class="form__col form__col--6 form__checkbox">
               <input type="checkbox">
               <label>Por acto de terceros</label>
           </div>
       </div>
        <div class="form__group">
           <div class="form__col form__col--6 form__checkbox">
               <input type="checkbox">
               <label>Por otros factores</label>
           </div>
           <div class="form__col form__col--6 form__checkbox">
               <input type="checkbox" class="add__item">
               <label>Precisar</label>
           </div>
       </div>

       <div class="form__group evento__detallar">
           <div class="form__col form__col--12">
               <input type="number" name="" class="form__input" placeholder="Detallar">
           </div>
       </div>  
       <div class="form__group">
           <div class="form__col form__col--12">
               <textarea class="form__textarea" placeholder="Escribe aquí"></textarea>
           </div>
       </div>
   </form>
</div>

@endsection
