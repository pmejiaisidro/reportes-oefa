
<div id="reporte-preliminar" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="report active modal__report">
                    <div class="report__content">
                        <div class="report__title">
                            <h2>Formato Nº 1</h2>
                            <h1>REPORTE PRELIMINAR DE EMERGENCIAS AMBIENTALES - 001</h1>
                        </div>
                        <h4>1. Datos del Administrado</h4>
                        <form>
                            <div class="form__group">
                                <div class="form__col form__col--12">
                                    <h6>Nombre o razón social</h6>
                                    <p>PETROLEOS DEL PERU PETROPERU SA</p>
                                </div>
                            </div>
                            <div class="form__group">
                                <div class="form__col form__col--6  ">
                                    <h6>Subsector</h6>
                                    <p>Hidrocarburos</p>
                                </div>
                                <div class="form__col form__col--6">
                                    <h6>Actividad</h6>
                                    <p>Exploracion</p>
                                </div>
                            </div>
                            <div class="form__group">
                                <div class="form__col form__col--12">
                                    <h6>Domicilio legal</h6>
                                    <p>Av. Enrique Canaval Moreyra Nro. 150</p>
                                </div>
                            </div>
                            <div class="form__group">
                                <div class="form__col form__col--6">
                                    <h6>Distrito</h6>
                                    <p>San Isidro</p>
                                </div>
                                <div class="form__col form__col--6">
                                    <h6>Provincia / Departamento"</h6>
                                    <p>Lima, Perú</p>
                                </div>
                            </div>
                            <div class="form__group">
                                <div class="form__col form__col--6 form__ver">
                                    <h6>Personas de contacto </h6>
                                    <p>Velasquez Salazar German Adolfo</p>
                                    <p>salaza@petro.peru.com</p>
                                    <p>945783196</p>
                                </div>
                            </div>
                        </form>
                        <h4>2. Del evento</h4>
                        <form>
                            <div class="form__group">
                                <div class="form__col form__col--12">
                                    <h6>Nombre de la instalación</h6>
                                    <p>lote 1033</p>
                                </div>
                            </div>
                            <div class="form__group">
                                <div class="form__col form__col--4">
                                    <h6>Fecha</h6>
                                    <p>14/05/2019</p>
                                </div>
                                <div class="form__col form__col--4">
                                    <h6>Hora de Inicio</h6>
                                    <p>16:00</p>
                                </div>
                                <div class="form__col form__col--4">
                                    <h6>Hora de Término</h6>
                                    <p>21:00</p>
                                </div>
                            </div>
                            <div class="form__group">
                                <div class="form__col form__col--6">
                                    <h6>Área Afectada</h6>
                                    <p>Lote 192</p>
                                </div>
                                <div class="form__col form__col--6">
                                    <h6>Cantidad derramada</h6>
                                    <p>4 millones de litros </p>
                                </div>
                            </div>
                            <div class="form__group ">
                                <div class="form__col form__col--6">
                                    <h6>Coordenadas UTMDATUM WGS84</h6>
                                    <p>24892 - Este</p>
                                </div>
                                <div class="form__col form__col--6 flex__end">
                                    <p>235892 - Oeste</p>
                                </div>
                            </div>
                            <div class="form__group">
                                <div class="form__col form__col--4">
                                    <h6>Localidad</h6>
                                    <p>Andina</p>
                                </div>
                                <div class="form__col form__col--4">
                                    <h6>Zona</h6>
                                    <p>Andoas</p>
                                </div>
                                <div class="form__col form__col--4">
                                    <h6>Distrito</h6>
                                    <p>Loreto</p>
                                </div>
                            </div>
                            <div class="form__group">
                                <div class="form__col form__col--6">
                                    <h6>Provincia</h6>
                                    <p>Datem del Marañón</p>
                                </div>
                                <div class="form__col form__col--6">
                                    <h6>Departamento</h6>
                                    <p>Lurin</p>
                                </div>
                            </div>
                            <div class="form__group">
                                <div class="form__col form__col--12">
                                    <h6 class="space">Posible origen de la Emergencia Ambiental</h6>
                                    <p>Por falla humana</p>
                                </div>
                            </div>
                            <div class="form__group">
                                <div class="form__col form__col--12">
                                    <h6>Descripción del evento</h6>
                                    <p>Por mala manipulación de un operario</p>
                                </div>
                            </div>
                            <div class="form__group">
                                <div class="form__col form__col--12">
                                    <h6>Características del área afectada y los componentes posiblemente afectados (aire, agua, suelo)</h6>
                                    <p>agua</p>
                                </div>
                            </div>
                        </form>
                        <h4>3. DE LA PERSONA QUE REPORTA</h4>
                        <form>
                            <div class="form__group ">
                                <div class="form__col form__col--12">
                                    <h6>Nombre y Apellidos</h6>
                                    <p>Guillermo Felix Bergelund Seminario</p>
                                </div>
                            </div>
                            <div class="form__group ">
                                <div class="form__col form__col--6">
                                    <h6>DNI o CE</h6>
                                    <p>78768934</p>
                                </div>
                                <div class="form__col form__col--6">
                                    <h6>Teléfono</h6>
                                    <p>9678468659</p>
                                </div>
                            </div>
                            <div class="form__group ">
                                <div class="form__col form__col--6">
                                    <h6>
                                        Cargo
                                    </h6>
                                    <p> Gerente general</p>
                                </div>
                                <div class="form__col form__col--6">
                                    <h6>Firma</h6>
                                    <p>Guillermo</p>
                                </div>
                            </div>
                        </form>

                        <form>
                            <div class="form__col form__col--12 content--imgAll">
                                <h4>4. EVIDENCIAS QUE SUSTENTAN EL REPORTE</h4>
                                <img src="https://img.elcomercio.pe/files/ec_article_multimedia_gallery/uploads/2018/06/05/5b17231288d6d.jpeg" alt="">
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="reporte-final" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="report active modal__report">
                    <div class="report__content">
                        <div class="report__title">
                            <h2>Formato Nº 2</h2>
                            <h1>REPORTE FINAL DE EMERGENCIAS AMBIENTALES - 001</h1>
                        </div>
                        <h4>1. Datos del Administrado</h4>
                        <form>
                            <div class="form__group">
                                <div class="form__col form__col--12">
                                    <h6>Nombre o razón social</h6>
                                    <p>PETROLEOS DEL PERU PETROPERU SA</p>
                                </div>
                            </div>
                            <div class="form__group">
                                <div class="form__col form__col--6  ">
                                    <h6>Subsector</h6>
                                    <p>Hidrocarburos</p>
                                </div>
                                <div class="form__col form__col--6">
                                    <h6>Actividad</h6>
                                    <p>Exploracion</p>
                                </div>
                            </div>
                            <div class="form__group">
                                <div class="form__col form__col--12">
                                    <h6>Domicilio legal</h6>
                                    <p>Av. Enrique Canaval Moreyra Nro. 150</p>
                                </div>
                            </div>
                            <div class="form__group">
                                <div class="form__col form__col--6">
                                    <h6>Distrito</h6>
                                    <p>San Isidro</p>
                                </div>
                                <div class="form__col form__col--6">
                                    <h6>Provincia / Departamento"</h6>
                                    <p>Lima, Perú</p>
                                </div>
                            </div>
                            <div class="form__group">
                                <div class="form__col form__col--6 form__ver">
                                    <h6>Personas de contacto </h6>
                                    <p>Velasquez Salazar German Adolfo</p>
                                    <p>salaza@petro.peru.com</p>
                                    <p>945783196</p>
                                </div>
                            </div>
                        </form>
                        <h4>2. Del evento</h4>
                        <form>
                            <div class="form__group">
                                <div class="form__col form__col--4">
                                    <h6>Fecha</h6>
                                    <p>14/05/2019</p>
                                </div>
                                <div class="form__col form__col--4">
                                    <h6>Hora de Inicio</h6>
                                    <p>16:00</p>
                                </div>
                                <div class="form__col form__col--4">
                                    <h6>Hora de Término</h6>
                                    <p>21:00</p>
                                </div>
                            </div>
                            <div class="form__group">
                                <div class="form__col form__col--12">
                                    <h6>Lugar donde ocurrió</h6>
                                    <p>lote 1033</p>
                                </div>
                            </div>
                            <div class="form__group">
                                <div class="form__col form__col--4">
                                    <h6>Localidad</h6>
                                    <p>Andina</p>
                                </div>
                                <div class="form__col form__col--4">
                                    <h6>Sector</h6>
                                    <p>Hidrocarburos</p>
                                </div>
                                <div class="form__col form__col--4">
                                    <h6>Distrito</h6>
                                    <p>Loreto</p>
                                </div>
                            </div>
                            <div class="form__group">
                                <div class="form__col form__col--6">
                                    <h6>Provincia</h6>
                                    <p>Maynas</p>
                                </div>
                                <div class="form__col form__col--6">
                                    <h6>Departamento</h6>
                                    <p>Loreto</p>
                                </div>
                            </div>
                            <div class="form__group ">
                                <div class="form__col form__col--12">
                                    <h6>Descripción detallada del evento</h6>
                                    <p>El evento ocurrió el día 5 de marzo a las 5:25 pm, la maquinaria que transportaba el petróleo crudo realizó una mala manipulación que le costó la volcadura del mismo. Hubo un derrame de aproximadamente 500 m3, se reportó la emergencia de inmediato para proceder con el reporte y la remediación de la zona afectada.</p>
                                </div>
                            </div>
                            <div class="form__group ">
                                <div class="form__col form__col--12">
                                    <h6>Causas que originaron el evento</h6>
                                    <p>Algunas de las causas fueron las siguientes: el personal no recibió la capacitación adecuada para trasladar esta materia prima, no se le dió a conocer la cantidad máxima que debió usar. Se asigna a un solo operario, y el traslado se realizó en épocas de lluvias sabiendo la peligrosidad de la situación.</p>
                                </div>
                            </div>
                            <div class="form__group ">
                                <div class="form__col form__col--12">
                                    <h6>Describir las condiciones climáticas durante y después de ocurrido el evento</h6>
                                    <p>No se observó un cambio en las condiciones climáticas que sea representativa.</p>
                                </div>
                            </div>
                            <div class="form__group ">
                                <div class="form__col form__col--12">
                                    <h6>¿Se puso en marcha el Plan de Contingencias?</h6>
                                    <p>Sí. Apenas ocurrido el evento se procedió a implementar el Plan de Contingencia, el cual se designó al grupo encargado, se limitó el área afectada y se procedió a realizar la remediación del sitio.</p>
                                </div>
                            </div>
                        </form>
                        <h4>3. Consecuencias del evento</h4>
                        <form>
                            <div class="form__group ">
                                <div class="form__col form__col--12">
                                    <h6>Impactos y/o daños ambientales</h6>
                                    <p>Hubo un impacto negativo en el recurso suelo, debido a la filtración del petróleo este alcanzó a penetrar cerca al río Amazonas, perjudicando la presencia de los peces en este río. </p>
                                </div>
                            </div>
                            <div class="form__group ">
                                <div class="form__col form__col--12">
                                    <h6>Afectación a la salud de las personas de los impactos y/o daños ambientales</h6>
                                    <p>El derrame afectó progresivamente la salud de las personas que viven alrededor, debido al fuerte olor, ocasionando dificultad para respirar.</p>
                                </div>
                            </div>
                            <div class="form__group ">
                                <div class="form__col form__col--12 form__ver">
                                    <h6>Derrame o fuga</h6>
                                    <p>Petróleo - Líquido</p>
                                    <p>Metano - Gaseoso</p>
                                </div>
                            </div>
                            <br>
                            <div class="form__group ">
                                <div class="form__col form__col--12 form__ver">
                                    <h6>Productos derramados</h6>
                                    <p>Petróleo -> Volumen derramado: 100 galones aprox. 
                                    <br>
                                    Área involucrada aproximadamente: 200 m2
                                    </p>
                                    <br>
                                    <p>Mercurio -> Volumen derramado: 100 litros aprox. 
                                    <br>
                                    Área involucrada aproximadamente: 100 m2
                                    </p>
                                </div>
                            </div>
                            <br>
                            <div class="form__group ">
                                <div class="form__col form__col--12 form__ver">
                                    <h6>Detalle las acciones realizadas por el administrado</h6>
                                    <p>- Se cercó el área afectada</p>
                                    <p>- Se implementaron barreras de contención para evitar el esparcimiento del producto derramado</p>
                                    <p>- Se realizó la remediación de la zona afectada.</p>
                                </div>
                            </div>
                            <br>
                            <div class="form__group ">
                                <div class="form__col form__col--12 form__ver">
                                    <h6>Cantidad de la sustancia, material o residuo recuperado:</h6>
                                    <p>500 m2</p>
                                </div>
                            </div>
                            <br>
                            <div class="form__group ">
                                <div class="form__col form__col--12 form__ver">
                                    <h6>Cantidad de la sustancia, material o residuo NO recuperado:</h6>
                                    <p>240 m2</p>
                                </div>
                            </div>
                        </form>
                        <form>
                            <div class="form__group ">
                                <div class="form__col form__col--12 form__ver">
                                    <h4>4. Acciones correctivas (Para corregir y/o evitar el evento descrito y sus consecuencias)</h4>
                                    <p>1. Capacitar al personal para el manejo adecuado de estos productos</p>
                                    <p>2. Asignar mayor personal para el traslado de estos productos</p>
                                </div>
                            </div>
                            <div class="form__group ">
                                <div class="form__col form__col--12 form__ver">
                                    <h4>5. Empresa Prestadora de Servicios de Residuos Sólidos (Transporte y Disposición final)</h4>
                                    <p>Outsourcing green S.A.</p>
                                </div>
                            </div>
                            <div class="form__group">
                                <div class="form__col form__col--12 form__ver">
                                    <h4>6. Estado de la instalación o medio de transporte después de la emergencia</h4>
                                    <p>Inoperativo parcial</p>
                                </div>
                            </div>
                            <div class="form__group">
                                <div class="form__col form__col--12 content--imgAll modal__img">
                                    <h4>7. Documentación que se adjunta</h4>
                                    <p style="margin: 0;">Croquis del lugar</p>
                                    <img src="http://geoportal.regionloreto.gob.pe/wp-content/uploads/2016/10/MEM_DPT1ONP_mini.jpg" alt="">
                                    <p>Otras fotografías</p>
                                    <img src="https://e.rpp-noticias.io/normal/2019/07/06/001500_812091.jpg">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>