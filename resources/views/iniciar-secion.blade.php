@extends('layouts.app')

<div class="container">
    <div class="login">
        <div class="login__content">
            <div class="login__logo">
                <img src="https://i.postimg.cc/cJx0tRQ9/oefa-logo.png" alt="">
            </div>
            <div class="login__box">
                <ul class="login__tabs">
                    <li class="active">
                        <a class="contact__hipervinculo contact__hipervinculo--active" data-box=".contact__iniciar">Iniciar sesión</a>
                    </li>
                    <li>
                        <a class="contact__hipervinculo "  data-box=".contact__registro">Registrarme</a>
                    </li>
                </ul>
                <div class="login__body">
                    <div class="login__body--card contact__place  contact__active contact__iniciar" style="display: block;"> 
                       <form>
                           <div class="form__group">
                               <div class="form__col form__col--12">
                                   <input type="number" name="" class="form__input" placeholder="Ingrese su RUC">
                               </div>
                            </div>
                            <div class="form__group">
                               <div class="form__col form__col--12">
                                   <input type="password" name="" class="form__input" placeholder="Ingrese su contraseña">
                               </div>
                            </div>
                            <div class="form__group">
                               <div class="form__col form__col--12 flex flex__jc">
                                    <button class="form__btn">ENVIAR</button>
                               </div>           
                           </div>
                       </form> 
                    </div>
                    <div class="login__body--card contact__place  contact__registro">
                       <form>
                           <div class="form__group">
                               <div class="form__col form__col--12">
                                   <input type="number" name="" class="form__input" placeholder="Ingrese su RUC">
                               </div>
                            </div>
                            <div class="form__group">
                               <div class="form__col form__col--12">
                                   <input type="email" name="" class="form__input" placeholder="Ingrese su correo">
                               </div>
                            </div>
                            <div class="form__group">
                               <div class="form__col form__col--12 flex flex__jc">
                                    <button class="form__btn">SOLICITAR CONTRASEÑA</button>
                               </div>           
                           </div>
                       </form>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>