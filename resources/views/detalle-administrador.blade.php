@extends('layouts.app')

@section('content')
<div class="text-body container">
    <h1>
        A & M TRANSPORTES S.R.L.
    </h1>
    <p><strong>Dirección:</strong> Mz G Lt 26 República Federal Alemana</p>
    <p><strong>RUC:</strong> 10741506446</p>
    <p><strong>Teléfono:</strong> 928 881 493</p>
    <p><strong>Correo institucional:</strong> grifo@gmail.com</p>
    <div class="box-piesite">
        <ul>
            <li class="per-38">
                <div class="piesite" id="pie_0" data-pie="38"></div>
                <div class="desc wow fadeIn">
                    <!-- <div class="parragraph">Porcentaje de Hidrocarburo </div> -->
                    <div class="number">Hidrocarburo</div>
                </div>
            </li>
            <li class="per-36">
                <div class="piesite" id="pie_1" data-pie="36"></div>
                <div class="desc wow fadeIn">
                    <!-- <div class="parragraph">Minería</div> -->
                    <div class="number">Minería</div>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="table">
  <div class="table-header">
    <div class="table-header__item table-25"><div><h5>Reporte</h5><div class="table-triangle table-triangle--asc"></div><div class="table-triangle table-triangle--des"></div></div><input type="text" id="js_input" placeholder="Escriba aquí"/></div>
    <div class="table-header__item table-40">
      <h5>Fecha</h5>
      <div class="table-date">
        <div class="table-date__title">Desde</div>
        <div class="table-date__content">
          <input id="searchFrom" class="searchInput" type="text" placeholder="mes/año"/>
        </div>
        <div class="table-date__title">Hasta</div>
        <div class="table-date__content">
          <input id="searchTo" class="searchInput" type="text" placeholder="mes/año"/>
         </div>
      </div>
    </div>
    <div class="table-header__item table-25">
        <h5>Sector</h5>
        <div class="form__col form__col--12 flex flex__jc form__select--box">
            <select class="form__select" id="js_select_sector">
                <option value="0">Seleccione aquí</option>
                <option value="Hidrocarburos">Hidrocarburos</option>
                <option value="Residuos sólidos">Residuos sólidos</option>
                <option value="Pesquería">Pesquería</option>
                <option value="Agricultura">Agricultura</option>
            </select>
        </div>
    </div>
    <div class="table-header__item table-25">
        <h5>Actividad</h5>
        <div class="form__col form__col--12 flex flex__jc form__select--box">
            <select class="form__select" id="js_select_actividad">
                <option value="0">Seleccione aquí</option>
                <option value="Transporte">Transporte</option>
                <option value="Comercialización UME">Comercialización UME</option> 
                <option value="Área Degradada">Área Degradada</option>
                <option value="Disposición Final">Disposición Final</option> 
                <option value="Cultivo de Trucha">Cultivo de Trucha</option> 
                <option value="Campo de cultivo">Campo de cultivo</option> 
            </select>
        </div>
    </div>
 </div>
 <div class="table-body">
   <div class="table-body__item js_table_body">
     <div class="table-25 table-body__item-c"><div data-toggle="modal" style="cursor:pointer;" data-target="#reporte-preliminar">R-P-001</div></div>
     <div class="table-40 table-body__item-c js_table_date">07/2019</div>
      <div class="table-25 table-body__item-c">Hidrocarburos</div>
      <div class="table-25 table-body__item-c">Transporte</div>
    </div>
    <div class="table-body__item js_table_body">
     <div class="table-25 table-body__item-c"><div data-toggle="modal" style="cursor:pointer;" data-target="#reporte-final">R-F-001</div></div>
     <div class="table-40 table-body__item-c js_table_date">07/2019</div>
      <div class="table-25 table-body__item-c">Hidrocarburos</div>
      <div class="table-25 table-body__item-c">Transporte</div>
    </div>
    <div class="table-body__item js_table_body">
     <div class="table-25 table-body__item-c"><div data-toggle="modal" style="cursor:pointer;" data-target="#reporte-preliminar">R-P-002</div></div>
     <div class="table-40 table-body__item-c js_table_date">04/2019</div>
      <div class="table-25 table-body__item-c">Hidrocarburos</div>
      <div class="table-25 table-body__item-c">Comercialización UME</div>
    </div>
   <div class="table-body__item js_table_body">
     <div class="table-25 table-body__item-c"><div data-toggle="modal" style="cursor:pointer;" data-target="#reporte-preliminar">R-F-002</div></div>
     <div class="table-40 table-body__item-c js_table_date">03/2018</div>
      <div class="table-25 table-body__item-c">Residuos sólidos</div>
      <div class="table-25 table-body__item-c">Disposición Final</div>
    </div>
    <div class="table-body__item js_table_body">
     <div class="table-25 table-body__item-c"><div data-toggle="modal" style="cursor:pointer;" data-target="#reporte-preliminar">R-P-003</div></div>
     <div class="table-40 table-body__item-c js_table_date">03/2018</div>
      <div class="table-25 table-body__item-c">Pesquería</div>
      <div class="table-25 table-body__item-c">Cultivo de Trucha</div>
    </div>
    <div class="table-body__item js_table_body">
     <div class="table-25 table-body__item-c"><div data-toggle="modal" style="cursor:pointer;" data-target="#reporte-preliminar">R-F-003</div></div>
     <div class="table-40 table-body__item-c js_table_date">02/2018</div>
      <div class="table-25 table-body__item-c">Agricultura</div>
      <div class="table-25 table-body__item-c">Campo de cultivo</div>
    </div>
  </div>
</div>
@include('reporte')

@endsection
